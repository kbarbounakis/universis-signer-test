import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.router.RouterNanoHTTPD;

import java.io.*;

public class FileStreamHandler extends RouterNanoHTTPD.DefaultStreamHandler {

    private final String file;
    private final String mimeType;

    public FileStreamHandler(String file, String mimeType) {
        this.file = file;
        this.mimeType = mimeType;
    }

    public String getMimeType() {
        return this.mimeType;
    }

    public NanoHTTPD.Response.IStatus getStatus() {
        return NanoHTTPD.Response.Status.OK;
    }

    public InputStream getData() {
        try {
            return new FileInputStream(this.file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}

import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.router.RouterNanoHTTPD;

/**
 * Handles 405 Method not allowed error
 */
public class MethodNotAllowedHandler extends RouterNanoHTTPD.DefaultHandler {
    public String getText() {
        return "<html><body><h3>Error 405: Method not allowed.</h3></body></html>";
    }

    public String getMimeType() {
        return "text/html";
    }

    public NanoHTTPD.Response.IStatus getStatus() {
        return NanoHTTPD.Response.Status.METHOD_NOT_ALLOWED;
    }
}

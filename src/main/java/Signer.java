import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfSignatureAppearance;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.security.BouncyCastleDigest;
import com.itextpdf.text.pdf.security.MakeSignature;
import com.itextpdf.text.pdf.security.PrivateKeySignature;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.w3c.dom.css.Rect;

import java.io.*;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Enumeration;

/**
 * Signs pdf documents
 */
public class Signer {

    private final KeyStore _keyStore;

    public Signer(KeyStore keyStore) {
        this._keyStore = keyStore;
    }

    /**
     * Signs the given input stream which represents a pdf document
     * @param inputStream - The input stream
     * @param outputStream - The output stream
     * @param thumbprint - The thumbprint of the certificate that is going to used for signing
     * @param password - The password of the private key that is going to used for signing
     * @param reason - A string which contains the reason of pdf signing, normally a description of a document. This string will be visible in pdf signature block
     * @throws IOException
     * @throws DocumentException
     * @throws GeneralSecurityException
     */
    public void sign(InputStream inputStream, OutputStream outputStream, String thumbprint, String password, String reason, Rectangle position) throws IOException, DocumentException, GeneralSecurityException {
        // create reader
        PdfReader reader = new PdfReader(inputStream);
        // create signature
        PdfStamper stamper = PdfStamper.createSignature(reader, outputStream, '\0');
        // get signature appearance
        PdfSignatureAppearance appearance = stamper.getSignatureAppearance();
        if (reason != null) {
            appearance.setReason(reason);
        }
        // todo: add image
        // ImageInstance imageInstance = Image.getInstance(inLogo)
        // appearance.setImage(imageInstance)
        // set signature position
        Rectangle finalPosition = new Rectangle(50, 10, 290, 100);
        if (position != null) {
            finalPosition = position;
        }
        appearance.setVisibleSignature(finalPosition, 1, "sig");
        BouncyCastleProvider provider = new BouncyCastleProvider();
        Security.addProvider(provider);
        String providerName = provider.getName();
        // open keystore and sign
        KeyStore ks = this._keyStore;
        // get keystore aliases
        Enumeration<String> aliases = ks.aliases();
        // enumerate keyStore items
        String alias = null;
        while (aliases.hasMoreElements()) {
            alias = aliases.nextElement();
            Certificate cert = ks.getCertificate(alias);
            if (cert instanceof X509Certificate) {
                String certThumbprint = X509CertificateInfo.getThumbprint((X509Certificate)cert);
                if (thumbprint.equals(certThumbprint)) {
                    break;
                }
            }
        }
        if (alias == null) {
            throw new CertificateException("The specified certificate cannot be found");
        }
        // get private key
        final Key key = ks.getKey(alias, password.toCharArray());
        // get cert
        final Certificate[] chain = ks.getCertificateChain(alias);
        // create digest algorithm
        String digestAlgorithm = com.itextpdf.text.pdf.security.DigestAlgorithms.SHA256;
        PrivateKeySignature pks = new PrivateKeySignature((PrivateKey) key, digestAlgorithm, providerName);
        BouncyCastleDigest digest = new BouncyCastleDigest();
        // and finally sign pdf
        MakeSignature.signDetached(appearance, digest, pks, chain, null, null, null, 0, MakeSignature.CryptoStandard.CMS);

    }

    /**
     * Signs the given pdf document
     * @param inFile - An absolute path of the input document
     * @param outFile - An absolute path of the output document
     * @param thumbprint - The thumbprint of the certificate that is going to used for signing
     * @param password - The password of the private key that is going to used for signing
     * @param reason - A string which contains the reason of pdf signing, normally a description of a document. This string will be visible in pdf signature block
     * @throws IOException
     * @throws DocumentException
     * @throws GeneralSecurityException
     */
    public void sign(String inFile, String outFile, String thumbprint, String password, String reason, Rectangle position) throws IOException, DocumentException, GeneralSecurityException {
        this.sign(new FileInputStream(inFile), new FileOutputStream(outFile), thumbprint, password, reason, position);
    }

}

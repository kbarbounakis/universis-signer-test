/**
 *  Signer application configuration options
 */
public class SignerAppConfiguration {
    /**
     * A string which contains the absolute path of a pkcs12 key store
     */
    public String keyStore;
    /**
     * A string which contains the keystore password
     */
    public String keyStorePass;
}

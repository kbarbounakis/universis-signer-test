
import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.router.RouterNanoHTTPD;
import fi.iki.elonen.util.ServerRunner;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

/**
 * Signer application
 */
public class SignerApp extends RouterNanoHTTPD {

    private static final String DEFAULT_HOST = "localhost";
    private static final int DEFAULT_PORT = 2465;

    protected SignerAppConfiguration configuration;

    public SignerApp() throws IOException {
        super(SignerApp.DEFAULT_HOST, SignerApp.DEFAULT_PORT);
        this.addMappings();
    }

    public SignerApp(SignerAppConfiguration configuration) throws IOException {
        super(SignerApp.DEFAULT_HOST, SignerApp.DEFAULT_PORT);
        this.configuration = configuration;
        this.addMappings();
    }

    public static void main(String[] args) {
        try {

            SignerAppConfiguration configuration = new SignerAppConfiguration();
            int findIndex = Arrays.binarySearch(args, "-keystore");
            if (findIndex >= 0 && args.length > findIndex) {
                // get keystore path
                configuration.keyStore = new java.io.File(args[findIndex + 1]).getAbsolutePath();
            }
            SignerApp server = new SignerApp(configuration);
            server.start(NanoHTTPD.SOCKET_READ_TIMEOUT, false);
        } catch (Exception e) {
            System.err.println("Couldn't start signer app:\n" + e);
        }
    }

    @Override
    public void addMappings() {
        super.addMappings();
        addRoute("/", RootHandler.class);
        addRoute("/keystore/certs", KeyStoreHandler.class, this.configuration);
        addRoute("/sign/", SignerHandler.class, this.configuration);
    }

}

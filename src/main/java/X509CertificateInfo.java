import javax.xml.bind.DatatypeConverter;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.Date;

/**
 * A set of attributes of a X509 certificate
 */
public class X509CertificateInfo {
    public int version;
    public String subjectDN;
    public String sigAlgName;
    public String sigAlgOID;
    public String issuerDN;
    public BigInteger serialNumber;
    public Date notAfter;
    public Date notBefore;
    public boolean expired;
    public String thumbprint;

    public X509CertificateInfo(X509Certificate cert) throws CertificateEncodingException, NoSuchAlgorithmException {
        version = cert.getVersion();
        subjectDN = cert.getSubjectDN().getName();
        sigAlgName = cert.getSigAlgName();
        sigAlgOID = cert.getSigAlgOID();
        issuerDN = cert.getIssuerDN().getName();
        serialNumber = cert.getSerialNumber();
        notAfter = cert.getNotAfter();
        notBefore = cert.getNotBefore();
        expired = !(notAfter.after(new Date()) && notBefore.before(new Date()));
        thumbprint = X509CertificateInfo.getThumbprint(cert);
    }

    public static String getThumbprint(X509Certificate cert)
            throws NoSuchAlgorithmException, CertificateEncodingException {
        MessageDigest md = MessageDigest.getInstance("SHA-1");
        byte[] der = cert.getEncoded();
        md.update(der);
        byte[] digest = md.digest();
        String digestHex = DatatypeConverter.printHexBinary(digest);
        return digestHex.toLowerCase();
    }

}

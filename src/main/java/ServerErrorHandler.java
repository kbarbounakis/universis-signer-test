import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.router.RouterNanoHTTPD;

/**
 * Handles 500 Internal Server Error
 */
public class ServerErrorHandler extends RouterNanoHTTPD.DefaultHandler {

    private String message = "Internal Server Error";
    public ServerErrorHandler(String message) {
        this.message = message;
    }

    public ServerErrorHandler() {
    }

    public String getText() {
        return "<html><body><h3>Error 500: " + this.message +  ".</h3></body></html>";
    }

    public String getMimeType() {
        return "text/html";
    }

    public NanoHTTPD.Response.IStatus getStatus() {
        return NanoHTTPD.Response.Status.FORBIDDEN;
    }
}

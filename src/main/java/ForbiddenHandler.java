import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.router.RouterNanoHTTPD;

/**
 * Handles 403 Forbidden error
 */
public class ForbiddenHandler extends RouterNanoHTTPD.DefaultHandler {
    public String getText() {
        return "<html><body><h3>Error 403: Forbidden.</h3></body></html>";
    }

    public String getMimeType() {
        return "text/html";
    }

    public NanoHTTPD.Response.IStatus getStatus() {
        return NanoHTTPD.Response.Status.FORBIDDEN;
    }
}

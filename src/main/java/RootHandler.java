import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.router.RouterNanoHTTPD;

/**
 * Handles / request
 */
public class RootHandler extends RouterNanoHTTPD.DefaultHandler {

    public String getText() {
        return "<html><body><h1>Universis Signer Application</h1></body></html>\n";
    }

    public String getMimeType() {
        return "text/html";
    }

    public NanoHTTPD.Response.IStatus getStatus() {
        return NanoHTTPD.Response.Status.OK;
    }
}

import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.router.RouterNanoHTTPD;
import org.apache.commons.codec.binary.Base64;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.*;

/**
 * Handlers /keystore/certs requests and returns an array of available certificates
 */
public class KeyStoreHandler implements RouterNanoHTTPD.UriResponder  {
    public NanoHTTPD.Response get(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        // get authorization header
        String[] usernamePassword;
        String authorizationHeader = ihttpSession.getHeaders().get("authorization");
        if (authorizationHeader != null && authorizationHeader.startsWith("Basic ")) {
            byte[] decodedBytes = Base64.decodeBase64(authorizationHeader.replaceFirst("Basic ", ""));
            usernamePassword = new String(decodedBytes).split(":");
        } else {
            return new ForbiddenHandler().get(uriResource, map, ihttpSession);
        }
        try {
            SignerAppConfiguration configuration = uriResource.initParameter(SignerAppConfiguration.class);
            if (configuration == null) {
                return new ServerErrorHandler("Application configuration cannot be empty at this context").get(uriResource, map, ihttpSession);
            }
            if (configuration.keyStore == null) {
                return new ServerErrorHandler("Invalid application configuration. Keystore cannot be empty at this context").get(uriResource, map, ihttpSession);
            }
            // load key store
            KeyStore ks = KeyStore.getInstance("pkcs12");
            ks.load(new FileInputStream(configuration.keyStore), usernamePassword[1].toCharArray());
            Enumeration<String> arr = ks.aliases();
            ArrayList<X509CertificateInfo> certs = new ArrayList<X509CertificateInfo>();
            // enumerate keyStore items
            while (arr.hasMoreElements()) {
                String alias = arr.nextElement();
                Certificate cert = ks.getCertificate(alias);
                if (cert instanceof X509Certificate) {
                    // create certificate information
                    X509CertificateInfo info = new X509CertificateInfo((X509Certificate)cert);
                    // add to collection
                    certs.add(info);
                }
            }
            // and finally return a JSON array
            return new JsonResponseHandler(certs).get(uriResource, map, ihttpSession);
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public NanoHTTPD.Response put(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return new MethodNotAllowedHandler().get(uriResource, map, ihttpSession);
    }

    public NanoHTTPD.Response post(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return new MethodNotAllowedHandler().get(uriResource, map, ihttpSession);
    }

    public NanoHTTPD.Response delete(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return new MethodNotAllowedHandler().get(uriResource, map, ihttpSession);
    }

    public NanoHTTPD.Response other(String s, RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return new MethodNotAllowedHandler().get(uriResource, map, ihttpSession);
    }
}

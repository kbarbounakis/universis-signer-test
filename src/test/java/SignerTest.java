
import com.itextpdf.text.DocumentException;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.*;

class SignerTest {

    @org.junit.jupiter.api.Test
    void sign() throws GeneralSecurityException, IOException, DocumentException {
        // load test keystore
        KeyStore ks = KeyStore.getInstance("pkcs12");
        String keyStorePath = SignerTest.class.getResource("keystore.p12").getPath();
        ks.load(new FileInputStream(keyStorePath), "secret".toCharArray());
        // create class
        Signer signer = new Signer(ks);
        // get inFile
        String inFile = SignerTest.class.getResource("loremipsum.pdf").getPath();
        String outFile = new java.io.File( "tmp/signed.pdf" ).getAbsolutePath();
        signer.sign(inFile, outFile, "d33ac6cc6290bcfb4a23243af0ec98b4f49b2238", "secret", null, null);
    }
}